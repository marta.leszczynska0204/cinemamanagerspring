import configuration.AppConfig;
import model.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.CustomerService;
import service.idGenerators.IdGeneratorCustomer;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class CustomerServiceTest {

    private static final Customer EXPECTED_USER_1 = new Customer("John Smith", "john.smith@epam.com", LocalDate.now().minusYears(20), null);
    private static final Customer EXPECTED_USER_2 = new Customer("Emily Berg", "emily.berg@epam.com", LocalDate.now().minusYears(40), null);
    private static final Customer EXPECTED_USER_3 = new Customer("Mat Eleen", "mat.eleen@epam.com", LocalDate.now().minusYears(33), null);

    @Autowired
    private ApplicationContext context;
    private CustomerService service;

    @Before
    public void setUp() {
        service = context.getBean(CustomerService.class);
    }

    @Test
    public void getByEmail() throws Exception {
        service.save(EXPECTED_USER_1);

        Customer customer = service.getByEmail(EXPECTED_USER_1.getEmailAddress());

        assertThat(customer.getName(), equalTo(EXPECTED_USER_1.getName()));
        assertThat(customer.getBirthDay(), equalTo(EXPECTED_USER_1.getBirthDay()));
        assertThat(customer.getEmailAddress(), equalTo(EXPECTED_USER_1.getEmailAddress()));
        assertThat(customer.getTickets(), equalTo(EXPECTED_USER_1.getTickets()));


        service.remove(customer);
    }

    @Test
    public void save() {
        Customer customer = service.save(EXPECTED_USER_1);

        assertThat(service.getAll().size(), equalTo(1));

        service.remove(customer);
    }

    @Test
    public void remove() {
        assertThat(service.getAll().size(), equalTo(0));
        Customer customer = service.save(EXPECTED_USER_1);

        assertThat(service.getAll().size(), equalTo(1));
        service.remove(customer);

        assertThat(service.getAll().size(), equalTo(0));
    }

    @Test
    public void getById() throws Exception {
        long userId = IdGeneratorCustomer.generateId();

        Customer customer = service.getById(userId);

        assertThat(customer.getName(), equalTo(EXPECTED_USER_1.getName()));
        assertThat(customer.getBirthDay(), equalTo(EXPECTED_USER_1.getBirthDay()));
        assertThat(customer.getEmailAddress(), equalTo(EXPECTED_USER_1.getEmailAddress()));
        assertThat(customer.getTickets(), equalTo(EXPECTED_USER_1.getTickets()));

        service.remove(customer);
    }

}
