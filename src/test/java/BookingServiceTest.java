import configuration.AppConfig;
import model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import repository.MovieRepository;
import repository.TicketRepository;
import service.BookingService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class BookingServiceTest {

    private static final Customer CUSTOMER_1 = new Customer("John Smith", "john.smith@epam.com", LocalDate.now().minusYears(20), null);
    private static final Customer CUSTOMER_2 = new Customer("Emily Berg", "emily.berg@epam.com", LocalDate.now().minusYears(40), null);

    private static final LocalDateTime AIRDATE = LocalDateTime.now().plusDays(5);
    private static final NavigableSet<LocalDateTime> AIRDATES = new TreeSet<>(Collections.singletonList(AIRDATE));
    private static final NavigableMap<LocalDateTime, Cinema> CINEMAS = new TreeMap<LocalDateTime, Cinema>() {

        {
            put(AIRDATE, new Cinema("Cinema", 30, 1));
        }
    };

    private static final Movie MOVIE_MID_RATING = new Movie("The Movie", AIRDATES, MovieRate.HIGH, CINEMAS);
    private static final Movie MOVIE_HIGH_RATING = new Movie("The Movie", AIRDATES, MovieRate.HIGH, CINEMAS);


    private static final Ticket EXPECTED_STANDARD_TICKET = new Ticket(CUSTOMER_1, MOVIE_MID_RATING, AIRDATE, 1);
    private static final Ticket EXPECTED_VIP_TICKET = new Ticket(CUSTOMER_1, MOVIE_MID_RATING, AIRDATE, 2);

    @Autowired
    private ApplicationContext context;
    @Autowired
    private BookingService service;
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private MovieRepository movieRepository;

    @Before
    public void setUp() {
        service = context.getBean(BookingService.class);
        ticketRepository.save(EXPECTED_STANDARD_TICKET);
        ticketRepository.save(EXPECTED_VIP_TICKET);
    }

    @After
    public void after() {
        movieRepository.removeAll();
        ticketRepository.removeAll();
    }


    @Test
    public void getTicketsPrice() throws Exception {
        double vipTicketsPriceMidRating = service.getTicketsPrice(MOVIE_MID_RATING, AIRDATE, CUSTOMER_1, Collections.singleton(ticketRepository.getAll().size()));
        double regularTicketsPriceMidRating = service.getTicketsPrice(MOVIE_MID_RATING, AIRDATE, CUSTOMER_1, Collections.singleton(ticketRepository.getAll().size()));

        double vipTicketsPriceHighRating = service.getTicketsPrice(MOVIE_HIGH_RATING, AIRDATE, CUSTOMER_1, Collections.singleton(ticketRepository.getAll().size()));
        double regularTicketsPriceHighRating = service.getTicketsPrice(MOVIE_HIGH_RATING, AIRDATE, CUSTOMER_1, Collections.singleton(ticketRepository.getAll().size()));

        double regularBirthdayTicketsPriceMidRating = service.getTicketsPrice(MOVIE_HIGH_RATING, AIRDATE, CUSTOMER_2, Collections.singleton(ticketRepository.getAll().size()));
        double vipBirthdayTicketsPriceMidRating = service.getTicketsPrice(MOVIE_MID_RATING, AIRDATE, CUSTOMER_2, Collections.singleton(ticketRepository.getAll().size()));

        double regularOverTenTicketsPriceMidRating = service.getTicketsPrice(MOVIE_MID_RATING, AIRDATE, CUSTOMER_2, Collections.singleton(ticketRepository.getAll().size()));

        double allMixedTicketsPriceHighRating = service.getTicketsPrice(MOVIE_HIGH_RATING, AIRDATE, CUSTOMER_2, Collections.singleton(ticketRepository.getAll().size()));

        assertThat(vipTicketsPriceMidRating).isEqualTo(50.0);
        assertThat(regularTicketsPriceMidRating).isEqualTo(25.0);

        assertThat(vipTicketsPriceHighRating).isEqualTo(60.0);
        assertThat(regularTicketsPriceHighRating).isEqualTo(90.0);

        assertThat(regularBirthdayTicketsPriceMidRating).isEqualTo(23.75);
        assertThat(vipBirthdayTicketsPriceMidRating).isEqualTo(47.5);

        assertThat(regularOverTenTicketsPriceMidRating).isEqualTo(237.5);

        assertThat(allMixedTicketsPriceHighRating).isEqualTo(300);
    }


    @Test
    public void getPurchasedTicketsForEvent() throws Exception {
        movieRepository.save(MOVIE_MID_RATING);
        Movie movie = movieRepository.getByName(MOVIE_MID_RATING.getName());

        EXPECTED_STANDARD_TICKET.setMovie(movie);
        EXPECTED_VIP_TICKET.setMovie(movie);
        service.bookTicket(CUSTOMER_1, EXPECTED_VIP_TICKET);

        Collection<Ticket> purchasedTicketsForEvent = service.getPurchasedTicketsForEvent(movie,
                AIRDATE);

        assertThat(purchasedTicketsForEvent.size()).isEqualTo(2);

        ticketRepository.removeAll();
        movieRepository.removeAll();
    }

}

