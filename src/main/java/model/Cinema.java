package model;

import com.google.common.base.MoreObjects;

import java.io.Serializable;
import java.util.Objects;

public class Cinema implements Serializable {
    private String name;
    private int numberOfRegularSeats;
    private int numberOfVIPSeats;

    public Cinema() {

    }

    public Cinema(String name, int numberOfRegularSeats, int numberOfVIPSeats) {
        this.name = name;
        this.numberOfRegularSeats = numberOfRegularSeats;
        this.numberOfVIPSeats = numberOfVIPSeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfRegularSeats() {
        return numberOfRegularSeats;
    }

    public void setNumberOfRegularSeats(int numberOfRegularSeats) {
        this.numberOfRegularSeats = numberOfRegularSeats;
    }

    public int getNumberOfVIPSeats() {
        return numberOfVIPSeats;
    }

    public void setNumberOfVIPSeats(int numberOfVIPSeats) {
        this.numberOfVIPSeats = numberOfVIPSeats;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("name", name).add("numberOfRegularSeats", numberOfRegularSeats)
                .add("vipSeats", numberOfVIPSeats).toString();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cinema cinema = (Cinema) o;
        return numberOfRegularSeats == cinema.numberOfRegularSeats &&
                numberOfVIPSeats == cinema.numberOfVIPSeats &&
                Objects.equals(name, cinema.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, numberOfRegularSeats, numberOfVIPSeats);
    }
}
