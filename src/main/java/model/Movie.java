package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Movie extends DomainObject {
    private Long id;
    private String name;
    private MovieRate rate;
    private transient NavigableSet<LocalDateTime> airDates = new TreeSet<>();
    private transient NavigableMap<LocalDateTime, Cinema> cinemas = new TreeMap<>();
    private Collection<MovieInfo> movieInfos;
    private Ticket ticket;

    public Movie() {

    }

    public Movie(String name, NavigableSet<LocalDateTime> airDates, MovieRate rate,
                 NavigableMap<LocalDateTime, Cinema> cinemas) {
        this.name = name;
        this.airDates = airDates;
        this.rate = rate;
        this.cinemas = cinemas;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieRate getRate() {
        return rate;
    }

    public void setRate(MovieRate rate) {
        this.rate = rate;
        if (rate == MovieRate.HIGH) {
            getTicket().setPrice(30.0);
        }
    }

    public Collection<MovieInfo> getMovieInfos() {
        return movieInfos;
    }

    public void setMovieInfos(Collection<MovieInfo> movieInfos) {
        this.movieInfos = movieInfos;
    }

    public boolean assignCinema(LocalDateTime dateTime, Cinema cinema) {
        if (airDates.contains(dateTime)) {
            cinemas.put(dateTime, cinema);
            return true;
        } else {
            return false;
        }
    }

    public boolean removeCinemaAssignment(LocalDateTime dateTime) {
        return cinemas.remove(dateTime) != null;
    }

    public boolean addAirDateTime(LocalDateTime dateTime) {
        return airDates.add(dateTime);
    }

    public boolean addAirDateTime(LocalDateTime dateTime, Cinema cinema) {
        boolean result = airDates.add(dateTime);
        if (result) {
            cinemas.put(dateTime, cinema);
        }
        return result;
    }

    public boolean removeAirDateTime(LocalDateTime dateTime) {
        boolean result = airDates.remove(dateTime);
        if (result) {
            cinemas.remove(dateTime);
        }
        return result;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public boolean airsOnDateTime(LocalDateTime dateTime) {
        return airDates.stream().anyMatch(localDateTime -> localDateTime.equals(dateTime));
    }

    public boolean airsOnDate(LocalDate date) {
        return airDates.stream().anyMatch(dt -> dt.toLocalDate().equals(date));
    }

    public NavigableSet<LocalDateTime> getAirDates() {
        return airDates;
    }

    public void setAirDates(NavigableSet<LocalDateTime> airDates) {
        this.airDates = airDates;
    }

    public NavigableMap<LocalDateTime, Cinema> getCinemas() {
        return cinemas;
    }

    public void setCinemas(NavigableMap<LocalDateTime, Cinema> cinemas) {
        this.cinemas = cinemas;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rate=" + rate +
                ", airDates=" + airDates +
                ", cinemas=" + cinemas +
                ", ticket=" + ticket +
                '}';
    }
}
