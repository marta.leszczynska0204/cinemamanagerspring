package model;

public enum MovieRate {
    LOW, MEDIUM, HIGH;
}
