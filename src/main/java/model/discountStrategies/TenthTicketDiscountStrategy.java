package model.discountStrategies;

import model.Customer;
import model.Movie;

import java.time.LocalDateTime;

public class TenthTicketDiscountStrategy implements DiscountStrategy{

    public TenthTicketDiscountStrategy() {

    }

    public int[] calculateDiscount(Customer customer, Movie movie, LocalDateTime airDateTime, int numberOfTickets) {
        if (numberOfTickets < 10) {
            return new int[numberOfTickets];
        }

        int[] discounts = new int[numberOfTickets];
        for (int i = 0; i < numberOfTickets; i++) {
            if ((i + 1) % 10 == 0) {
                discounts[i] = 50;
            }
        }

        return discounts;
    }

}
