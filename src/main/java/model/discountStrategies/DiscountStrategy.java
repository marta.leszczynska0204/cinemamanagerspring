package model.discountStrategies;

import model.Customer;
import model.Movie;

import java.time.LocalDateTime;

public interface DiscountStrategy {
    int[] calculateDiscount(Customer user, Movie event, LocalDateTime airDateTime, int numberOfTickets);
}

