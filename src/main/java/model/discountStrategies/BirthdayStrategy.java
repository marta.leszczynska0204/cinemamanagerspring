package model.discountStrategies;

import model.Customer;
import model.Movie;

import java.time.LocalDateTime;

public class BirthdayStrategy implements DiscountStrategy{
    public int[] calculateDiscount(Customer customer, Movie movie, LocalDateTime airDateTime, int numberOfTickets) {
        if (customer.getBirthDay().withYear(airDateTime.getYear()).isBefore(airDateTime.toLocalDate().plusDays(5))
                && customer.getBirthDay().withYear(airDateTime.getYear()).isAfter(airDateTime.toLocalDate().minusDays(5))) {
            int[] discounts = new int[numberOfTickets];
            for (int i = 0; i < numberOfTickets; i++) {
                discounts[i] = 5;
            }
            return discounts;
        }
        return new int[numberOfTickets];
    }

}
