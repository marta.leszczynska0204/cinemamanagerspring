package model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;

public class Customer extends DomainObject {
    private Long id;
    private String name;
    private String emailAddress;
    private LocalDate birthDay;
    private Collection<Ticket> tickets = new TreeSet<>();

    public Customer() {

    }

    public Customer(String name, String emailAddress, LocalDate birthDay, NavigableSet<Ticket> tickets) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.birthDay = birthDay;
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Collection<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Collection<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", birthDay=" + birthDay +
                ", tickets=" + tickets +
                '}';
    }
}
