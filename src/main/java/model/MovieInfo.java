package model;

import java.time.LocalDateTime;

public class MovieInfo {
    private Movie movie;
    private String cinemaKey;
    private LocalDateTime airDate;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getCinemaKey() {
        return cinemaKey;
    }

    public void setCinemaKey(String CinemaKey) {
        this.cinemaKey = cinemaKey;
    }

    public LocalDateTime getAirDate() {
        return airDate;
    }

    public void setAirDate(LocalDateTime airDate) {
        this.airDate = airDate;
    }

}
