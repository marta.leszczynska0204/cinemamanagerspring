package model;

import java.time.LocalDateTime;

public class Ticket extends DomainObject implements Comparable<Ticket> {
    private Movie movie;
    private LocalDateTime date;
    private Long seat;
    private Customer customer;
    private Double price = 20.0;
    private String discount;
    private Cinema cinema;
    private MovieRate movieRate;

    public Ticket (){

    }
    public Ticket(Customer customer, Movie movie, LocalDateTime date, long seat) {
        this.customer = customer;
        this.movie = movie;
        this.date = date;
        this.seat = seat;
    }


    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getSeat() {
        return seat;
    }

    public void setSeat(Long seat) {
        this.seat = seat;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public MovieRate getMovieRate() {
        return movieRate;
    }

    public void setMovieRate(MovieRate movieRate) {
        this.movieRate = movieRate;
    }


    @Override
    public String toString() {
        return "Ticket{" +
                "movie=" + movie +
                ", date=" + date +
                ", seat=" + seat +
                ", customer=" + customer +
                ", price=" + price +
                ", discount='" + discount + '\'' +
                ", movieRate=" + movieRate +
                '}';
    }

    @Override
    public int compareTo(Ticket otherTicket) {
        if (otherTicket == null) {
            return 1;
        }
        int result = date.compareTo(otherTicket.getDate());

        if (result == 0) {
            result = movie.getName().compareTo(otherTicket.getMovie().getName());
        }
        if (result == 0) {
            result = Long.compare(seat, otherTicket.getSeat());
        }
        return result;
    }

}

