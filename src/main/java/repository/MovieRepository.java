package repository;

import model.Movie;
import model.MovieInfo;
import model.MovieRate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Repository
public class MovieRepository extends AbstractRepository<Movie> {

    @Override
    public Movie save(Movie movie) {
        String query;
        Object[] parameters;
        if (movie.getId() == null) {
            query = "INSERT INTO " + getTableName() + "name, rating) VALUES (?, ?, ?)";
            parameters = new Object[]{movie.getName(), movie.getRate().toString()};
        } else {
            query = "UPDATE " + getTableName() + " SET name = ?, rating = ? WHERE id = ?";
            parameters = new Object[]{movie.getName(), movie.getRate().toString(), movie.getId()};
        }
        jdbcTemplate.update(query, parameters);

        return movie;
    }

    public Movie getByName(String name) {
        String query = "SELECT * FROM " + getTableName() + " e where e.name = ?";
        final Movie movie = jdbcTemplate.queryForObject(query, getRowMapper(), name);

        return movie;
    }

    public Collection<Movie> getForDateRange(LocalDateTime fromDate, LocalDateTime toDate) {
        return getAll();
    }

    public Collection<Movie> getNextEvents(LocalDateTime toDate) {
        return getForDateRange(LocalDateTime.now(), toDate);
    }

    @Override
    public Movie getObjectFromRow(Map<String, Object> result) {
        Movie movie = new Movie();
        Long eventId = (Long) result.get("id");
        movie.setId(eventId);
        movie.setName((String) result.get("name"));
        movie.setRate(MovieRate.valueOf((String) result.get("rating")));

        movie.setMovieInfos(retrieveEventInfos(eventId));

        return movie;
    }

    @Override
    public String getTableName() {
        return MOVIE_TABLE;
    }

    @Override
    public RowMapper<Movie> getRowMapper() {
        return new RowMapper<Movie>() {
            @Override
            public Movie mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Movie movie = new Movie();
                Long eventId = resultSet.getLong("id");
                movie.setId(eventId);
                movie.setName(resultSet.getString("name"));
                movie.setRate(MovieRate.valueOf(resultSet.getString("rating")));

                movie.setMovieInfos(retrieveEventInfos(eventId));

                return movie;
            }
        };
    }

    private Collection<MovieInfo> retrieveEventInfos(Long eventId) {
        return Collections.emptyList();
    }

}