package repository;

import model.DomainObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

abstract class AbstractRepository<T extends DomainObject> implements Repository<T> {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    public abstract T save(T object);

    public void remove(T object) {
        String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        jdbcTemplate.update(query, object.getId());
    }

    @Override
    public T getById(Long id) {
        String query = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, getRowMapper());
    }

    @Override
    public Collection<T> getAll() {
        List<Map<String, Object>> queryResult = jdbcTemplate.queryForList("SELECT * FROM " + getTableName());
        return queryResult.stream().map(this::getObjectFromRow).collect(Collectors.toList());
    }

    public abstract T getObjectFromRow(Map<String, Object> result);

    public abstract String getTableName();

    public abstract RowMapper<T> getRowMapper();

    public void removeAll() {
        jdbcTemplate.update("DELETE FROM " + getTableName());
    }

}