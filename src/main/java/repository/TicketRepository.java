package repository;

import model.Customer;
import model.Movie;
import model.Ticket;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class TicketRepository extends AbstractRepository<Ticket> {

    public Set<Ticket> save(Set<Ticket> tickets) {
        Set<Ticket> savedTickets = new HashSet<>();
        tickets.forEach(ticket -> savedTickets.add(save(ticket)));

        return savedTickets;
    }

    @Override
    public Ticket save(Ticket ticket) {
        String query;
        Object[] parameters;
        if (ticket.getId() == null) {
            query = "INSERT INTO " + getTableName() + "(datetime, seat, event_id, user_id) VALUES (?, ?, ?, ?)";
            parameters = new Object[]{ticket.getDate(), ticket.getSeat(),
                    ticket.getMovie() == null ? null : ticket.getMovie().getId(),
                    ticket.getCustomer() == null ? null : ticket.getCustomer().getId()};
        } else {
            parameters = new Object[]{ticket.getDate(), ticket.getSeat(),
                    ticket.getMovie() == null ? null : ticket.getMovie().getId(),
                    ticket.getCustomer() == null ? null : ticket.getCustomer().getId(), ticket.getId()};
            query =
                    "UPDATE " + getTableName() + " SET datetime = ?, seat = ?, event_id = ?, user_id = ? WHERE id = ?";
        }
        jdbcTemplate.update(query, parameters);

        return ticket;

    }

    @Override
    public Ticket getObjectFromRow(Map<String, Object> result) {
        Ticket ticket = new Ticket();
        ticket.setSeat((Long) result.get("seat"));

        Movie movie = new Movie();
        movie.setId((Long) result.get("event_id"));
        ticket.setMovie(movie);

        Customer customer = new Customer();
        customer.setId((Long) result.get("user_id"));
        ticket.setCustomer(customer);

        ticket.setDate(((Date) result.get("datetime")).toInstant().atZone(ZoneId.systemDefault())
                .toLocalDateTime());

        return ticket;

    }

    @Override
    public String getTableName() {
        return repository.Repository.TICKET_TABLE;
    }

    @Override
    public RowMapper<Ticket> getRowMapper() {
        return new RowMapper<Ticket>() {

            @Override
            public Ticket mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Ticket ticket = new Ticket();
                ticket.setSeat(resultSet.getLong("seat"));

                Movie movie = new Movie();
                movie.setId(resultSet.getLong("event_id"));
                ticket.setMovie(movie);
                ;

                Customer customer = new Customer();
                customer.setId(resultSet.getLong("user_id"));
                ticket.setCustomer(customer);

                ticket.setDate(resultSet.getDate("datetime").toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDateTime());

                return ticket;
            }
        };
    }


    public Collection<Ticket> getByEventAndTime(Movie movie, LocalDateTime dateTime) {
        Long movieId = movie.getId();

        String query = "SELECT t.* FROM " + getTableName() + " t"
                + " WHERE t.event_id = ?";

        List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(query, movie.getId());

        return queryResult.stream().map(this::getObjectFromRow).collect(Collectors.toList());
    }

}
