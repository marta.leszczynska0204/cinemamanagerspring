package repository;

import helper.Helper;
import model.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

@Repository
public class CustomerRepository extends AbstractRepository<Customer> {

    public Customer getCustomerByEmail(String email) {
        final String query = "SELECT * FROM user u where u.email = ?";
        final Customer customer = jdbcTemplate.queryForObject(query, getRowMapper(), email);

        return customer;

    }

    public Customer save(Customer customer) {
        Date birthdate = Date.from(customer.getBirthDay().atStartOfDay(ZoneId.systemDefault()).toInstant());

        String query;
        Object[] parameters;
        if (customer.getId() != null) {
            query = "UPDATE " + getTableName() + " SET birthdate = ?,"
                    + "email = ?, firstname = ?, lastname = ?" + " WHERE id = ?";
            parameters = new Object[]{birthdate, customer.getEmailAddress(), customer.getName(), customer.getId()};
        } else {
            query = "INSERT INTO " + getTableName() + "(birthdate, email, firstname, lastname) VALUES (?,?,?,?)";
            parameters = new Object[]{birthdate, customer.getEmailAddress(), customer.getName()};
        }

        jdbcTemplate.update(query, parameters);

        return customer;

    }

    @Override
    public Customer getObjectFromRow(Map<String, Object> result) {
        Customer customer = new Customer();
        customer.setId((Long) result.get("id"));
        customer.setBirthDay(new Date(((Date) result.get("birthdate")).getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        customer.setEmailAddress((String) result.get("email"));
        customer.setName((String) result.get("name"));

        return customer;

    }

    @Override
    public String getTableName() {
        return CUSTOMER_TABLE;
    }

    @Override
    public RowMapper<Customer> getRowMapper() {
        return new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Customer customer = new Customer();
                customer.setId(resultSet.getLong("id"));
                customer.setBirthDay(Helper.dateToLocaDate(resultSet.getDate("birthdate")));
                customer.setEmailAddress(resultSet.getString("email"));
                customer.setName(resultSet.getString("tname"));

                return customer;
            }
        };
    }

}


