package repository;

import java.util.Collection;

public interface Repository<T> {
    String MOVIE_TABLE = "Movie";
    String MOVIE_MOVIE_INFO_TABLE = "Movie_MovieInfo";
    String MOVIE_INFO_TABLE = "MovieInfo";
    String TICKET_TABLE = "Ticket";
    String CUSTOMER_TABLE = "Customer";
    String CUSTOMER_TICKET_TABLE = "Customer_Ticket";

    T save(T object);

    void remove(T object);

    T getById(Long id);

    Collection<T> getAll();

}
