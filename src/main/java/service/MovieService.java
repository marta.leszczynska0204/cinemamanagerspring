package service;

import model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import repository.MovieRepository;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

public class MovieService {
    private MovieRepository repository;
    private Map<Long, Movie> movieMap;

    @Autowired
    public MovieService(MovieRepository repository) {
        this.repository = repository;
    }

    public MovieService() {

    }

    @Nullable
    public Movie getByName(@Nonnull String name) {
        return repository.getByName(name);
    }

    public void remove(@Nonnull Movie movie) {
        repository.remove(movie);
    }

    public Movie getById(@Nonnull Long id) {
        return repository.getById(id);
    }

    @Nonnull
    public Collection<Movie> getAll() {
        return repository.getAll();
    }

    public Collection<Movie> getNextEvents(LocalDateTime toDate) {
        return repository.getNextEvents(toDate);
    }

    public Collection<Movie> getForDateRange(LocalDateTime fromDate, LocalDateTime toDate) {
        return repository.getForDateRange(fromDate, toDate);
    }

    public void saveCustomerToList(Movie movie) {
        movieMap.put(movie.getId(), movie);
    }

    public Movie getCustomerById(int id) {
        return movieMap.get(id);
    }

    public Movie getMovieByName(String name) {
        Movie movie = new Movie();
        for (Movie m : movieMap.values()) {
            if (m.getName().equals(name)) {
                movie = m;
            }
        }
        return movie;
    }

    public Collection<Movie> getCustomerCollectionFromMap() {
        return movieMap.values();
    }
}
