package service;

import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import repository.TicketRepository;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BookingService {
    private Map<Customer, Ticket> purchasedTicketsAll;
    private Map<Customer, Ticket> bookedTicketsMap;

    private TicketRepository ticketRepository;
    private DiscountService discountService;


    public BookingService() {

    }

    @Autowired
    public BookingService(TicketRepository ticketRepository, DiscountService discountService) {
        this.ticketRepository = ticketRepository;
        this.discountService = discountService;
    }


    public double getTicketsPrice(@Nonnull Movie movie, @Nonnull LocalDateTime dateTime, @Nullable Customer customer,
                                  @Nonnull Set<Integer> seats) {
        int[] discounts = discountService.getDiscount(customer, movie, dateTime, seats.size());

        double totalPrice = 0;
        double basePrice = movie.getTicket().getPrice();

        Cinema cinema = movie.getCinemas().get(dateTime);
        MovieRate rating = movie.getRate();

        int index = 0;
        for (Integer seat : seats) {
            double price = basePrice;

            if (rating == MovieRate.HIGH) {
                price = 1.5 * price;
            }

            double discountAmount = price * discounts[index] / 100;
            price = price - discountAmount;

            totalPrice += price;

            index++;
        }

        return totalPrice;

    }

    public void bookTicket(Customer customer, Ticket ticket) {
        ticketRepository.save(ticket);
    }

    public Collection<Ticket> getPurchasedTicketsForEvent(@Nonnull Movie movie, @Nonnull LocalDateTime dateTime) {
        return ticketRepository.getByEventAndTime(movie, dateTime);
    }

}
