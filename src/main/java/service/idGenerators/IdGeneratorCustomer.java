package service.idGenerators;

public class IdGeneratorCustomer {
    private static long nextId = 0;

    public static long generateId() {
        nextId++;
        return nextId;
    }
}
