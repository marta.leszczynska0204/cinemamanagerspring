package service.idGenerators;

public class IdGeneratorMovie {
    private static long nextId = 0L;

    public static long generateId() {
        nextId++;
        return nextId;
    }
}
