package service;

import model.Customer;
import model.Movie;
import model.discountStrategies.DiscountStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import repository.TicketRepository;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.List;

public class DiscountService {
    private List<DiscountStrategy> discountStrategies;
    private TicketRepository ticketRepository;

    public DiscountService() {

    }

    @Autowired
    public DiscountService(List<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }


    public int[] getDiscount(@Nullable Customer customer, @NonNull Movie movie, @Nonnull LocalDateTime airDateTime, int numberOfTickets) {
        int[] discounts = new int[numberOfTickets];

        for (DiscountStrategy discountStrategy : discountStrategies) {
            int[] calculatedDiscounts = discountStrategy.calculateDiscount(customer, movie, airDateTime, numberOfTickets);
            for (int i = 0; i < numberOfTickets; i++) {
                if (calculatedDiscounts[i] > discounts[i]) {
                    discounts[i] = calculatedDiscounts[i];
                }
            }
        }

        return discounts;
    }

}
