package service;

import com.google.common.collect.ImmutableSet;
import model.Cinema;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CinemaService {

    @Value("${cinema1}")
    private String cinema1;
    @Value("${cinema2}")
    private String cinema2;
    @Value("${cinema3}")
    private String cinema3;

    public CinemaService() {

    }

    @NonNull
    public Set<Cinema> getAll() {
        ImmutableSet<String> cinemas = ImmutableSet.of(cinema1, cinema2, cinema3);
        return extractAuditoriums(cinemas);

    }

    private Set<Cinema> extractAuditoriums(Set<String> auditoriumStrings) {
        return auditoriumStrings.stream().map(this::getCinemaFromProperties).collect(Collectors.toSet());
    }

    private Cinema getCinemaFromProperties(String cinemaString) {
        String[] split = cinemaString.split(";");

        return new Cinema(split[0], (int) Integer.parseInt(split[1]), (int) Integer.parseInt(split[2]));
    }

    @NonNull
    public Cinema getCinemaByName(String name) {
        Optional<Cinema> cinemaOptional = getAll().stream().findAny();
        return cinemaOptional.get();
    }
}
