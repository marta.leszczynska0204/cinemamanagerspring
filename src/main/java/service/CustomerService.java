package service;

import model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import repository.CustomerRepository;

import javax.annotation.Nonnull;
import java.util.Collection;

@Service
public class CustomerService {
    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    @Nullable
    public Customer getByEmail(@Nonnull String email) {
        return repository.getCustomerByEmail(email);
    }

    public Customer save(@Nonnull Customer customer) {
        repository.save(customer);
        return customer;
    }

    public void remove(@Nonnull Customer customer) {
        repository.remove(customer);
    }

    public Customer getById(@Nonnull Long id) {
        return repository.getById(id);
    }

    @Nonnull
    public Collection<Customer> getAll() {
        return repository.getAll();
    }
}
