package aspects;

import com.google.common.collect.Maps;
import model.Movie;
import model.Ticket;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

import java.util.Map;
import java.util.Set;

@Aspect
public class CounterAspect {
    private final Map<String, Integer> eventAccessesByName = Maps.newHashMap();
    private final Map<String, Integer> eventPriceQueries = Maps.newHashMap();
    private final Map<String, Integer> eventTicketsBooked = Maps.newHashMap();

    @AfterReturning("execution(* service.MovieService.getByName(..))")
    public void eventByNameAdvice(JoinPoint joinPoint) {
        String eventName = (String) joinPoint.getArgs()[0];
        putResultsIntoMap(eventName, eventAccessesByName);
    }

    @AfterReturning("execution(* service.BookingService.getTicketsPrice(..))")
    public void eventPriceQueriesAdvice(JoinPoint joinPoint) {
        String eventName = ((Movie) joinPoint.getArgs()[0]).getName();
        putResultsIntoMap(eventName, eventPriceQueries);
    }

    @AfterReturning("execution(* service.BookingService.bookTicket(..))")
    public void eventBookedTicketsAdvice(JoinPoint joinPoint) {
        Set<Ticket> bookedTickets = (Set<Ticket>) joinPoint.getArgs()[0];
        bookedTickets.forEach(this::putTicketIntoEventTicketsBookedMap);
    }

    private void putTicketIntoEventTicketsBookedMap(Ticket ticket) {
        putResultsIntoMap(ticket.getMovie().getName(), eventTicketsBooked);
    }

    private void putResultsIntoMap(String eventName, Map<String, Integer> map) {
        Integer count = map.get(eventName);
        map.put(eventName, count == null ? 1 : count + 1);
    }

    public Map<String, Integer> getEventAccessesByName() {
        return eventAccessesByName;
    }

    public Map<String, Integer> getEventPriceQueries() {
        return eventPriceQueries;
    }

    public Map<String, Integer> getEventTicketsBooked() {
        return eventTicketsBooked;
    }

}
