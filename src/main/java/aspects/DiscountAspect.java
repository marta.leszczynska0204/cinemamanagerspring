package aspects;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import model.Customer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.Map;

@Aspect
public class DiscountAspect {

    @VisibleForTesting
    static final String BIRTHDAY_DISCOUNT = "Birthday Discount";
    @VisibleForTesting
    static final String TENTH_TICKET_DISCOUNT = "Tenth Ticket Discount";

    private final Map<String, Integer> discountsTotal = Maps.newHashMap();
    private final Map<Long, Map<String, Integer>> discountsPerCustomer = Maps.newHashMap();

    @Around("execution(* model.discountStrategies.BirthdayStrategy.calculateDiscount(..))")
    public int[] birthdayDiscountsTotalAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        return discountTotal(joinPoint, BIRTHDAY_DISCOUNT);
    }

    @Around("execution(* model.discountStrategies.TenthTicketStrategy.calculateDiscount(..))")
    public int[] tenthTicketDiscountsTotalAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        return discountTotal(joinPoint, TENTH_TICKET_DISCOUNT);
    }

    @Around("execution(* model.discountStrategies.BirthdayStrategy.calculateDiscount(..))")
    public int[] birthdayDiscountsPerUserAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        return discountsPerUser(joinPoint, BIRTHDAY_DISCOUNT);
    }

    @Around("execution(* model.discountStrategies.TenthTicketStrategy.calculateDiscount(..))")
    public int[] tenthTicketDiscountsPerUserAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        return discountsPerUser(joinPoint, TENTH_TICKET_DISCOUNT);
    }

    private int[] discountTotal(ProceedingJoinPoint joinPoint, String discountName) throws Throwable {
        try {
            final int[] discounts = (int[]) joinPoint.proceed();
            putIntoDiscountsTotalMap(discountName, discounts);
            joinPoint.proceed();

            return discounts;
        } catch (Throwable throwable) {
            throw throwable;
        }
    }

    private int[] discountsPerUser(ProceedingJoinPoint joinPoint, String discountName) throws Throwable {
        Customer customer = (Customer) joinPoint.getArgs()[0];
        Long userId = customer.getId();
        try {
            final int[] discounts = (int[]) joinPoint.proceed();
            putIntoDiscountsPerUserMap(userId, discountName, calculateNumberOfDiscounts(discounts));

            return discounts;
        } catch (Throwable throwable) {
            throw throwable;
        }
    }

    private void putIntoDiscountsTotalMap(String discountName, int[] discounts) {
        putIntoMap(discountName, calculateNumberOfDiscounts(discounts), discountsTotal);
    }


    private void putIntoMap(String discountName, int numberOfDiscounts, Map<String, Integer> map) {
        Integer count = map.get(discountName);
        map.put(discountName, count == null ? numberOfDiscounts : count + numberOfDiscounts);
    }

    private void putIntoDiscountsPerUserMap(Long customerId, String discountName, int numberOfDiscounts) {
        Map<String, Integer> customerDiscountsTotal = discountsPerCustomer.get(customerId);
        if (customerDiscountsTotal == null) {
            customerDiscountsTotal = Maps.newHashMap();
        }
        putIntoMap(discountName, numberOfDiscounts, customerDiscountsTotal);
        discountsPerCustomer.put(customerId, customerDiscountsTotal);
    }

    private int calculateNumberOfDiscounts(int[] discounts) {
        int numberOfDiscounts = 0;
        for (int discount : discounts) {
            if (discount > 0) {
                numberOfDiscounts++;
            }
        }

        return numberOfDiscounts;
    }

    public Map<String, Integer> getDiscountsTotal() {
        return discountsTotal;
    }

    public Map<Long, Map<String, Integer>> getDiscountsPerUser() {
        return discountsPerCustomer;
    }

}
