package configuration;

import aspects.CounterAspect;
import aspects.DiscountAspect;
import com.google.common.collect.ImmutableList;
import model.discountStrategies.BirthdayStrategy;
import model.discountStrategies.DiscountStrategy;
import model.discountStrategies.TenthTicketDiscountStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.List;

@Configuration
@Import(PropertiesConfig.class)
@ComponentScan({"repository", "service"})
@EnableAspectJAutoProxy
public class AppConfig {

    @Value("${database.url}")
    private String databaseUrl;

    @Value("${database.username}")
    private String databaseUsername;

    @Value("${database.password}")
    private String databasePassword;

    @Autowired
    private DiscountStrategy birthdayStrategy;

    @Autowired
    private DiscountStrategy tenthTicketStrategy;

    @Autowired
    private DataSource dataSource;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public DiscountStrategy birthdayStrategy() {
        return new BirthdayStrategy();
    }

    @Bean
    public DiscountStrategy tenthTicketStrategy() {
        return new TenthTicketDiscountStrategy();
    }

    @Bean
    public List<DiscountStrategy> discountStrategies() {
        return ImmutableList.of(birthdayStrategy, tenthTicketStrategy);
    }

    @Bean
    public CounterAspect counterAspect() {
        return new CounterAspect();
    }

    @Bean
    public DiscountAspect discountAspect() {
        return new DiscountAspect();
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DriverManagerDataSource dataSource() {
        return new DriverManagerDataSource(databaseUrl, databaseUsername, databasePassword);
    }


}



